%This scrip is used to generate several trace graph
%   GroundTruthTime : vector of size Ls x 1, Ls is the length of simulaiton sequence, which indicate all
%		    m target is sampled with same length  
%
%   GroundTruthXY : matrix of size Lgd x 2 x m , Lgd is the length of simulation equence, 2 is (x,y), and m is
%		    the number of targets
%
%   ExclusiveLocatingXY : matrix of size Le x 2 x m, Le is the length of Locating sequence , 2 is
%		    the dimension and m is the number of targetes 
%   ExclusiveLocatingTime : matrix of size Le x m, le the length of Locating sequence, m is the
%		    number of targets
%
%   ChorusLocatingXY :  matrix of size Lc x 2 x m, Lc is the length of locating sequence, 2 is
%		    the dimension and m is the number of targetes  
%   ChorusLocatingTime : ChorusLocatingTime is matrix of size Lc x m, Lc is the length of locaitng
%		    time, m is the number of targets
%
%   ChorusLocatingWPXY, matrxi of size Lc x 2 x m, Lc is the lenght of locating sequence, 2 is
%		    the dimension and m is the nubmer of targets
%   ChorusLocatingWPTime : ChorusLocatingWPTime is matrix of size Lc x m, Lc is the length of locating  
%		    time, m is the number of targets

clear; %clear the workspace
NoDisplay = 0;
cdfFilename = './cdfdata.txt';
fid = fopen(cdfFilename,'w');
%for Omega = [.1 .2 .5 1 1.5 2 5];
%for Noise = [0 0.01 0.05 0.1 0.2 0.5]; 
%for Omega = linspace(0.1,5,10.1);
%for Noise = linspace(0,0.5,10); 
for Omega = 0.03;
for Noise = 0.001; 
disp(['[Omega Noise] = ',num2str(Omega),num2str(Noise)]);

%Distribution of Receiver and motion area of targets
ReceiverBound = [-5 15 -5 15];
d = 2;
R = 5;
DeltaS =  sqrt(2)*R/ceil(sqrt(d+1));
[tx,ty] = meshgrid(ReceiverBound(1) : DeltaS : ReceiverBound(2), ReceiverBound(3) : DeltaS : ReceiverBound(4));
XYReceivers = [tx(:) ty(:)];

%Basic parameters about the motion 
MuV = 2; % mean velocity
SigmaV = 0.1; %sigma of velocity 
SamplingRate = 1000; %Sample rate sample/second
XBox = 10; % motion region 10 meters 
YBox = 10; % motion region 10 meters
TotalSec = 10; % simulation time
TurnSec = 3; % interval between random truning 
NumTargets = 7;
ColorArray = 'bgrcmyk';

%Generating and ploting GroundTruthXY and GroundTruthTime:
close all;
figure ();
hold on;
LegendArray = cell(NumTargets,1);
GroundTruthTime = 0 : 1/SamplingRate : TotalSec;
GroundTruthXY = zeros(length(GroundTruthTime),2,NumTargets);
UpdateTrace = 1;
for i = 1 : NumTargets
    if UpdateTrace
	[ x,y ] = GiveMeATrace(MuV,SigmaV, SamplingRate, XBox, YBox,TotalSec,TurnSec);
	GroundTruthXY(:,1,i) = x;
	GroundTruthXY(:,2,i) = y;
    end
    plot(GroundTruthXY(:,1,i),GroundTruthXY(:,2,i),[ColorArray(i),'-']);
    LegendArray{i} = ['Target ',num2str(i)];
end
legend(LegendArray,'Location','NorthEastOutside');
for i = 1 : NumTargets
    plot(GroundTruthXY(1,1,i),GroundTruthXY(1,2,i),[ColorArray(i),'^']); 
    plot(GroundTruthXY(end,1,i),GroundTruthXY(end,2,i),[ColorArray(i),'o']); 
end
%plot(XYReceivers(:,1),XYReceivers(:,2),'bo');
box on;
%FileName = '/Users/bean/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/GroundTruthXY';
FileName = '.\figure\GroundTruthXY';
xlabel('x(m)');
ylabel('y(m)');
sdf('pdf');
axis equal;
axis([0 XBox 0 YBox]);
if ~NoDisplay
    SavePDFto(FileName,gcf);
end

%DownSampling & calculate the location 
LocatingInterval = 100; % Locating every 100 sample

DownSampleSeq = 1 : LocatingInterval : length(GroundTruthTime);  
DownSampleTime = GroundTruthTime(DownSampleSeq);  

SlotEachTag = vec2mat(DownSampleSeq,NumTargets);
SlotEachTag(SlotEachTag == 0 ) = inf;
TimeEachTag = ones(size(SlotEachTag))*inf;
TimeEachTag(SlotEachTag<inf) = GroundTruthTime(SlotEachTag(SlotEachTag<inf));

% generation and ploting sparse sampled trace
figure;
hold on;
for i = 1 : NumTargets
    x = GroundTruthXY(DownSampleSeq,1,i) ;
    y = GroundTruthXY(DownSampleSeq,2,i) ;
    plot(x,y,[ColorArray(i),'-']);
    LegendArray{i} = ['Target ',num2str(i)];
end
legend(LegendArray,'Location','NorthEastOutside');

for i = 1 : NumTargets
    plot(GroundTruthXY(1,1,i),GroundTruthXY(1,2,i),[ColorArray(i),'^']); 
    plot(GroundTruthXY(end,1,i),GroundTruthXY(end,2,i),[ColorArray(i),'o']); 
end
box on;
%FileName = 'C:\Users\Lei\Documents\MyPapers\jasc2014-ultrachorus\fig\TraceInBox'
FileName = '.\figure\TraceInBox'
%FileName = '/Users/bean/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/SparseTraceInBox';
xlabel('x(m)');
ylabel('y(m)');
sdf('pdf');
axis equal;
if ~NoDisplay
    SavePDFto(FileName,gcf);
end
close all;


% plot exclusive locating result
ExclusiveLocatingXY = ones(size(SlotEachTag,1),2,NumTargets).*inf;
figure;
hold on;
for i = 1 : NumTargets
    ThisSlot = SlotEachTag(:,i);
    x = GroundTruthXY(ThisSlot(ThisSlot < inf),1,i) ;
    y = GroundTruthXY(ThisSlot(ThisSlot < inf),2,i) ;
    XYTargets = [x,y];
    DistMat = pdist2(XYTargets,XYReceivers)  ;
    DistMat = DistMat + rand(size(DistMat)).*Noise;
    for j = 1 : size(DistMat,1)
	InIdx = find(DistMat(i,:) < R);
	[XYResult,Flag,Res] = ToALoc(XYReceivers(InIdx,:),DistMat(j,InIdx)');
	ExclusiveLocatingXY(j,:,i) = XYResult';
    end
    plot(ExclusiveLocatingXY(ThisSlot<inf,1,i),ExclusiveLocatingXY(ThisSlot<inf,2,i),[ColorArray(i),'-']);
    LegendArray{i} = ['Target ',num2str(i)];
end
legend(LegendArray,'Location','NorthEastOutside');

for i = 1 : NumTargets
    plot(ExclusiveLocatingXY(1,1,i),ExclusiveLocatingXY(1,2,i),[ColorArray(i),'^']); 
    plot(ExclusiveLocatingXY(find(ExclusiveLocatingXY(:,1,i)<inf,1,'last'),1,i),ExclusiveLocatingXY(find(ExclusiveLocatingXY(:,2,i)<inf,1,'last'),2,i),[ColorArray(i),'o']); 
end
box on;
FileName = '.\figure\ExclusiveLocatingXY'
%FileName = '/Users/bean/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/ExclusiveLocatingXY';
xlabel('x(m)');
ylabel('y(m)');
sdf('pdf');
axis equal;
axis([0 XBox 0 YBox]);
if ~NoDisplay
    SavePDFto(FileName,gcf);
end
close all;

% Generating and ploting Chorus locating result
ChorusLocatingXY = ones(length(DownSampleSeq),2,NumTargets).*inf;
ChorusLocatingTime = ones(length(DownSampleSeq),NumTargets).*inf;
ResM = ones(length(DownSampleSeq),NumTargets).*inf;
FlagM = ones(length(DownSampleSeq),NumTargets).*inf;
ResSigmaForDrop = 1;
figure; 
hold on;
TCount = 2;
fprintf(1,'progress = 00');
lastt = DownSampleSeq(1);
ChorusLocatingXY(1,:,:) = GroundTruthXY(1,:,:);
ChorusLocatingTime(1,:) = repmat(GroundTruthTime(DownSampleSeq(1)),1,NumTargets);
FlagM(1,:) =  zeros(1,NumTargets);
ResM(1,:) =  zeros(1,NumTargets);
for t = DownSampleSeq(2:end)
    XYnow = zeros(NumTargets,2); %Locaiton slice of all target, in n x 2 targets
    XYLast = zeros(NumTargets,2); %Locaiton slice of all target, in n x 2 targets
    for i = 1 : NumTargets
	XYnow(i,:) = GroundTruthXY(t,:,i); %the locaiton of target i at time t
	%XYLast(i,:) = GroundTruthXY(lastt,:,i); %Locaiton slice of all target, in n x 2 targets
	XYLast(i,:) = ChorusLocatingXY(TCount-1,:,i); %Locaiton slice of all target, in n x 2 targets
    end
    DistMat = pdist2(XYnow,XYReceivers);
    DistMat = DistMat + rand(size(DistMat)).*Noise;
    [DirtyDistMat,RankMat] =  AnonyCollison(DistMat,Omega+0.1,R); %
    MotionRadius = MuV*(repmat(GroundTruthTime(t),1,NumTargets)-ChorusLocatingTime(TCount-1,:));
    [CurLoc, Residual,Flags] = ChorusLocating(XYLast,MotionRadius,DirtyDistMat,XYReceivers);
    FlagM(TCount,:) = Flags';
    ResM(TCount,:) = Residual';
    for i = 1 : NumTargets
	if ResM(TCount,i) < ResSigmaForDrop;
	    ChorusLocatingXY(TCount,:,i) = CurLoc(i,:);
	    ChorusLocatingTime(TCount,i) = GroundTruthTime(t);
	else
	    ChorusLocatingXY(TCount,:,i) =  ChorusLocatingXY(TCount-1,:,i);
	    ChorusLocatingTime(TCount,i) = ChorusLocatingTime(TCount-1,i);
	end
    end
    %ChorusLocatingXY(TCount,:,:) = CurLoc(:,:)';
    TCount = TCount + 1;
    progress = t/max(DownSampleSeq)*100;
    fprintf(1,'\b\b%02d',floor(progress)); 
    %disp(['Progress = ',num2str(progress)]);
    lastt = t;
end
fprintf(1,'\n');

for i = 1 : NumTargets
    plot(ChorusLocatingXY(:,1,i),ChorusLocatingXY(:,2,i),[ColorArray(i),'-']);
    LegendArray{i} = ['Target ',num2str(i)];
end
legend(LegendArray,'Location','NorthEastOutside');

for i = 1 : NumTargets
    plot(ChorusLocatingXY(1,1,i),ChorusLocatingXY(1,2,i),[ColorArray(i),'^']); 
    plot(ChorusLocatingXY(end,1,i),ChorusLocatingXY(end,2,i),[ColorArray(i),'o']); 
end
box on;
%FileName = '/Users/bean/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/ChorusLocatingXY';
FileName = '.\figure\ChorusLocatingXY';
xlabel('x(m)');
ylabel('y(m)');
sdf('pdf');
axis equal;
axis([0 XBox 0 YBox]);
if ~NoDisplay
    SavePDFto(FileName,gcf);
end
close all;

% Generating and ploting TOAC locating result
TOACLocatingXY = ones(length(DownSampleSeq),2,NumTargets).*inf;
TOACLocatingTime = ones(length(DownSampleSeq),NumTargets).*inf;
TOACResM = ones(length(DownSampleSeq),NumTargets).*inf;
FlagM = ones(length(DownSampleSeq),NumTargets).*inf;
ResSigmaForDrop = 1;
figure; 
hold on;
TCount = 2;
fprintf(1,'progress = 00');
lastt = DownSampleSeq(1);
TOACLocatingXY(1,:,:) = GroundTruthXY(1,:,:);
TOACLocatingTime(1,:) = repmat(GroundTruthTime(DownSampleSeq(1)),1,NumTargets);
FlagM(1,:) =  zeros(1,NumTargets);
TOACResM(1,:) =  zeros(1,NumTargets);
for t = DownSampleSeq(2:end)
    XYnow = zeros(NumTargets,2); %Locaiton slice of all target, in n x 2 targets
    XYLast = zeros(NumTargets,2); %Locaiton slice of all target, in n x 2 targets
    for i = 1 : NumTargets
	XYnow(i,:) = GroundTruthXY(t,:,i); %the locaiton of target i at time t
	XYLast(i,:) = TOACLocatingXY(TCount-1,:,i); %Locaiton slice of all target, in n x 2 targets
	%XYLast(i,:) = GroundTruthXY(TCount-1,:,i); %Locaiton slice of all target, in n x 2 targets
    end
    DistMat = pdist2(XYnow,XYReceivers);
    DistMat = DistMat + rand(size(DistMat)).*Noise;
    [DirtyDistMat,RankMat] =  AnonyCollison(DistMat,Omega,R); %
    MotionRadius = MuV*(repmat(GroundTruthTime(t),1,NumTargets)-TOACLocatingTime(TCount-1,:));
    [CurLoc, Residual,Flags] = ChorusLocating(XYLast,MotionRadius,DirtyDistMat,XYReceivers);
    FlagM(TCount,:) = Flags';
    TOACResM(TCount,:) = Residual';
    for i = 1 : NumTargets
	if TOACResM(TCount,i) < ResSigmaForDrop;
	    TOACLocatingXY(TCount,:,i) = CurLoc(i,:);
	    TOACLocatingTime(TCount,i) = GroundTruthTime(t);
	else
	    TOACLocatingXY(TCount,:,i) =  TOACLocatingXY(TCount-1,:,i);
	    TOACLocatingTime(TCount,i) = TOACLocatingTime(TCount-1,i);
	end
    end
    %ChorusLocatingXY(TCount,:,:) = CurLoc(:,:)';
    TCount = TCount + 1;
    progress = t/max(DownSampleSeq)*100;
    fprintf(1,'\b\b%02d',floor(progress)); 
    %disp(['Progress = ',num2str(progress)]);
    lastt = t;
end
fprintf(1,'\n');

for i = 1 : NumTargets
    plot(TOACLocatingXY(:,1,i),TOACLocatingXY(:,2,i),[ColorArray(i),'-']);
    LegendArray{i} = ['Target ',num2str(i)];
end
legend(LegendArray,'Location','NorthEastOutside');

for i = 1 : NumTargets
    plot(TOACLocatingXY(1,1,i),TOACLocatingXY(1,2,i),[ColorArray(i),'^']); 
    plot(TOACLocatingXY(end,1,i),TOACLocatingXY(end,2,i),[ColorArray(i),'o']); 
end
box on;
%FileName = '/Users/bean/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/ChorusLocatingXY';
FileName = '.\figure\TOACLocatingXY';
xlabel('x(m)');
ylabel('y(m)');
sdf('pdf');
axis equal;
axis([0 XBox 0 YBox]);
if ~NoDisplay
    SavePDFto(FileName,gcf);
end
close all;

%Calculating and ploting cdf
%FileDir = '/Users/bean/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/';
%FileDir = '/Users/bean/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/';
FileDir = '.\figure\';
ChorusErr = ones(length(DownSampleSeq) , NumTargets)*inf; 
ExclusiveErr = ones(length(DownSampleSeq), NumTargets)*inf;
TOACErr = ones(length(DownSampleSeq), NumTargets)*inf;

for i = 1 : NumTargets
    ThisFileName = [FileName,'Contrast',num2str(i),'Trace'];
    ThisSampleTime = TimeEachTag(TimeEachTag(:,i)<inf,i);
    figure ();
    hold on;
    gx = GroundTruthXY(:,1,i);
    gy = GroundTruthXY(:,2,i);
    plot(gx,gy,[ColorArray(1),'-']);
    ex = ExclusiveLocatingXY(TimeEachTag(:,i)<inf,1,i);
    ey = ExclusiveLocatingXY(TimeEachTag(:,i)<inf,2,i);
    plot(ex,ey,[ColorArray(2),'--']);
    cx = ChorusLocatingXY(:,1,i);
    cy = ChorusLocatingXY(:,2,i);
    ResV = ResM(:,i);
    ValidIdx = find(ResV < inf);
    plot(cx(ValidIdx),cy(ValidIdx),[ColorArray(3),'-.']);
    tx = TOACLocatingXY(:,1,i);
    ty = TOACLocatingXY(:,2,i);
    TOACResV = TOACResM(:,i);
    ValidIdx = find(TOACResV < inf);
    plot(tx(ValidIdx),ty(ValidIdx),[ColorArray(4),':']);
    legend('GroundTruth','In Exclusive','In Chorus','In Greedy-TOAC','Location','Best');
    xlabel('x(m)');
    ylabel('y(m)');
    sdf('pdf');
    axis equal;
    %axis([0 XBox 0 YBox]);
    if ~NoDisplay
	SavePDFto(ThisFileName,gcf);
    end

    iex = interp1(ThisSampleTime,ex,GroundTruthTime','linear');
    iey = interp1(ThisSampleTime,ey,GroundTruthTime','linear');

    icx = interp1(DownSampleTime,cx,GroundTruthTime','linear');
    icy = interp1(DownSampleTime,cy,GroundTruthTime','linear');

    itx = interp1(DownSampleTime,tx,GroundTruthTime','linear');
    ity = interp1(DownSampleTime,ty,GroundTruthTime','linear');

    ThisExclusiveErr = sqrt((gx - iex).^2 + (gy -iey).^2);
    ThisChorusErr = sqrt((gx - icx).^2 + (gy -icy).^2);
    ThisTOACErr = sqrt((gx - itx).^2 + (gy -ity).^2);
    ExclusiveErr(1:length(ThisExclusiveErr),i) = ThisExclusiveErr;
    ChorusErr(1:length(ThisChorusErr),i) = ThisChorusErr;
    TOACErr(1:length(ThisTOACErr),i) = ThisTOACErr;
end
ExclusiveErr = ExclusiveErr(:);
ChorusErr = ChorusErr(:);

close all;
figure ();
[Fe,Xe] = ecdf(ExclusiveErr(ExclusiveErr<inf)); 
[Fc,Xc] = ecdf(ChorusErr(ChorusErr< inf));
[Ft,Xt] = ecdf(TOACErr(TOACErr< inf));
maxx = max([Xe;Xc;Xt]);
plot([Xe;maxx],[Fe;1],'-',[Xc;maxx],[Fc;1],'-',[Xt;maxx],[Ft;1],'-');
axis([0 maxx 0 1.1]);
legend('In Exclusive','In Chorus','In Greedy-TOCA','Location','SouthEast');
xlabel('Err(m)');
ylabel('Ratio');
sdf('pdf');
%FileName = '/Users/bean/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/CDFContrast';
FileName = '.\figure\CDFContrast';
if ~NoDisplay
    SavePDFto(FileName,gcf);
end
close all;

% Output cdf data to file
fprintf(fid,'-1 %f\n',Noise);
fprintf(fid,'%f ',Xe);
fprintf(fid,'\n');
fprintf(fid,'%f ',Fe);
fprintf(fid,'\n');

fprintf(fid,'%f %f\n',Omega,Noise);
fprintf(fid,'%f ',Xc);
fprintf(fid,'\n');
fprintf(fid,'%f ',Fc);
fprintf(fid,'\n');
end
end
fclose(fid);
