close all;
hold on 
colorMap = hsv(4);
plot(Num,[Num*50;ChorusDelay*50;TOACDelay*50]);
[P S ] = polyfit(Num,TOACDelay*50,1);
y = P(1)*Num + P(2);
plot(Num,y,'--','color',colorMap(2,:));
%plot(Num,Num);
%legend();
sdf('pdf');
legend('Exclusive','Chorus','Greedy-TOAC','Polyfit','location','northwest');
xlabel('Number of targets');
ylabel('Locating interval(ms)');
SavePDFto('vsDelay',gcf);
