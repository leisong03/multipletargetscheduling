function [ conflictOnRx,conflictGraph ] = CalConflict(T,XYReceiver, XYTarget ,Radius, Omega)
    %SCHEDULING Summary of this function goes here
    %   Detailed explanation goes here
    %T = zeros( size(XYTarget,1),1);
    %FileName = '/Users/bean/Documents/MyPaper/[2015][W][Infocom]UltraChorusWithDelay/fig/ThisUDG'
    FileName = '.\figure\';
    %FileName = 'C:\Users\Lei\Documents\MyPapers\jasc2014-ultrachorus\fig\ThisUDG'
    DistMat = pdist2(XYTarget, XYReceiver);
    conflictGraph = zeros(size(XYTarget,1),size(XYTarget,1));
    A = sum(DistMat <= Radius)>1;
    conflictOn = zeros(size(A));
    conflictOnRx = cell(1,size(XYReceiver,1));
    for i = find(A)
	distToi = DistMat(:,i)+T'*.34;
	distDiff = bsxfun(@minus,distToi,distToi');
	conflictGraph = conflictGraph + (distDiff>0 & distDiff < Omega*0.34);
	[pairA pairB] = find(distDiff>0 & distDiff < Omega*.34);
	if nnz(distDiff>0 & distDiff < Omega*.34)
	    conflictOn(i) = conflictOn(i) + 1;
	    conflictOnRx{i} = [conflictOnRx{i};[pairA pairB]];
	    conflictPair = [pairA pairB];
	    [distToi(pairA),distToi(pairB),distToi(pairA)-distToi(pairB)]
	end
    end
end
