theta = 0:0.01:2*pi
theta = theta';
CircleBase = [cos(theta),sin(theta)];
Radius = [20,17,18];
Circle0 = CircleBase.*Radius(1);
Circle1 = CircleBase.*Radius(2) + repmat([0, 1.2],length(theta),1);
Circle2 = CircleBase.*Radius(3) + repmat([0, -1.1],length(theta),1);
plot(Circle0(:,1),Circle0(:,2),Circle1(:,1),Circle1(:,2),Circle2(:,1),Circle2(:,2));
axis equal;
sdf('pdf');
xlabel('x(m)');
ylabel('y(m)');
legend('Trace1','Trace2','Trace3');
saveas(gcf,[figdir,'3Traces'],'pdf')
command = ['/bin/bash', ' --login',' ./figure/a.sh',' ./3Traces.pdf']; 
system(command);

close all;
Velocity = 1;
Delta = 5;
ThetaV = Velocity*Delta./Radius;
theta = [0: ThetaV(1) :2*pi]';
CircleBase = [cos(theta),sin(theta)];
Circle0 = CircleBase.*Radius(1);
theta = [0: ThetaV(2) :2*pi]';
CircleBase = [cos(theta),sin(theta)];
Circle1 = CircleBase.*Radius(2) + repmat([0, 1.2],length(theta),1);
theta = [0: ThetaV(3) :2*pi]';
CircleBase = [cos(theta),sin(theta)];
Circle2 = CircleBase.*Radius(3) + repmat([0, -1.1],length(theta),1);
plot(Circle0(:,1),Circle0(:,2),'-o',Circle1(:,1),Circle1(:,2),'-o',Circle2(:,1),Circle2(:,2),'-o');
axis equal;
sdf('pdf');
xlabel('x(m)');
ylabel('y(m)');
legend('Trace1','Trace2','Trace3');
saveas(gcf,[figdir,'3TracesChorus'],'pdf')
command = ['/bin/bash', ' --login',' ./figure/a.sh',' ./3TracesChorus.pdf']; 
system(command);


