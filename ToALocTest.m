close all;
ColorStr = 'bgrcmyk';
FileName = '/Users/bean/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/LocAlgoCompare';
omega = 0.5
XOuter = 10;
YOuter = 10;
[tx ty] = meshgrid([0:3:XOuter],[0:3:YOuter]);
Radius = 3.2;
XYRx = [tx(:),ty(:)] + rand(size([tx(:),ty(:)]));
NumTag = 4;
XIn = 6;
YIn = 6;
XYTag = rand(NumTag,2).*repmat([XIn,YIn],NumTag,1)+repmat([XOuter-XIn YOuter-YIn]./2,NumTag,1);
DistMat = pdist2(XYTag,XYRx);
EstXYTag = zeros(size(XYTag));
for i = 1 : size(XYTag,1)
    IdxReachRx = find (DistMat(i,:) < Radius);
    [ThisEst,Flag,Res] = ToALoc(XYRx(IdxReachRx,:),DistMat(i,IdxReachRx)'); 
    EstXYTag(i,:) = ThisEst';
end
plot(XYTag(:,1),XYTag(:,2),'go',EstXYTag(:,1),EstXYTag(:,2),'rd')
axis equal;
axis([0 XOuter 0 YOuter]);
%legend('Real Postion','Locating result','Location','NorthEastOutside');
xlabel('X(m)');
ylabel('Y(m)');
legend('Real Postion','Locating result');
sdf('pdf_20');
SaveSuccess = SavePDFto(FileName, gcf);
close all;

theta = linspace(0,2*pi,50)';
hold on;
[DirtyMat,RankMat] = AnonyCollison(DistMat,omega,Radius)
for i = 1 : size(DistMat,2)
    for j = 1 : nnz(DirtyMat(:,i) < inf)
	XYCircle = DirtyMat(j,i).*[cos(theta),sin(theta)]+repmat(XYRx(i,:),length(theta),1);
	plot(XYCircle(:,1),XYCircle(:,2),[ColorStr(RankMat(j,i)),'-']);
    end
end
plot(XYTag(:,1),XYTag(:,2),'k^')
box on;
axis equal;
%axis([0 XOuter 0 YOuter]);
xlabel('X(m)');
ylabel('Y(m)');
FileName = '/Users/bean/Documents/MyPaper/[2015][W][Infocom]UltraChorusWithDelay/fig/TriSingle'

axis off;
sdf('pdf_20');
SaveSuccess = SavePDFto(FileName, gcf);
%close all;
