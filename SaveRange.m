function [ NumFigure, dGraph ] = SaveRange(XYReceivers, XYTargets,R, Omega )
%SAVERANGE Summary of this function goes here
%   Detailed explanation goes here
NumFigure = 0;
DistMat = zeros(size(XYTargets,1), size(XYReceivers,1));
for i = 1 : size(XYReceivers, 1);
    %Column by column
    for j = 1 : size(XYTargets,1)
	ThisDist =  norm(XYReceivers(i,:) - XYTargets(j,:));
	if ThisDist < R
	    DistMat(j,i) = ThisDist;
	end
    end
end

NonEmptyRXList = find(sum(DistMat) > 0)

CollisionOnReceiver = cell(size(XYReceivers,1),1);
for i = NonEmptyRXList
    CollisionPair = [];
    vec = DistMat(:,i)
    nzElem = find(vec > 0)
    RangeDiff = bsxfun(@minus,vec(nzElem),vec(nzElem)')

    uRangeDiff = triu(RangeDiff);
    [ua ub] = find(uRangeDiff < Omega & uRangeDiff>0 )

    lRangeDiff = tril(RangeDiff);
    [la lb] = find(lRangeDiff < Omega & lRangeDiff>0 )
     
    a = [ua;la];
    b = [ub;lb];

    if ~isempty(a)
	CollisionPair = [ nzElem(a),nzElem(b)];
    end
    CollisionOnReceiver{i} = CollisionPair;
end

handle = figure(); 
figdir = 'figure/';
DistMat
xname = cell(length(NonEmptyRXList),1);
count = 1;
for i = NonEmptyRXList
    xname{count} = ['RX',num2str(i)];
    count = count +1;
end
xname
xlegend = cell(size(XYTargets,1),1);
for i = 1 : size(XYTargets,1)
    xlegend{i} = ['Tag',num2str(i)];
end
bar2([1 : length(NonEmptyRXList)],DistMat(:,NonEmptyRXList),'BASE','XLABEL',xname);
legend(xlegend);
%axis([0 25 0 1]);
set(gcf,'PaperSize',[20,100]);
set(gcf,'Position',[0 0 4400 800]);
filename = 'AllReceiver';
%saveas(handle,[figdir,filename],'pdf')
sdf('pdf');
print(gcf,[figdir,filename,'.pdf'], '-dpdf','-r300');
command = ['/bin/bash', ' --login',' ./figure/a.sh',' ',filename,'.pdf']; 
system(command);
%for i = 1 : size(XYReceivers,1)
while 0
    clf(handle);
    xname = cell(size(XYTargets,1),1);
    for j = 1 : size(XYTargets,1)
	xname{j} = ['T',num2str(j)];
    end
    DistMat(:,i)
    XYTargets
    xname
    bar2([1 : size(XYTargets,1)],DistMat(:,i)','BASE','XLABEL',xname);
    filename = ['Receiver',num2str(i)];
    sdf('pdf');
    saveas(handle,[figdir,filename],'pdf')
    command = ['/bin/bash', ' --login',' ./figure/a.sh',' ',filename,'.pdf']; 
    system(command);
end
%clf(handle);
SurviveDistMat = zeros(size(XYTargets,1), size(XYReceivers,1));
ConflictDistMat = zeros(size(XYTargets,1), size(XYReceivers,1));

dGraph = zeros(size(XYTargets,1),size(XYTargets,1));
for  i = 1 : length(CollisionOnReceiver)
    if ~isempty(CollisionOnReceiver{i})
	thisCollision = CollisionOnReceiver{i}
	[NumCC, CCidx] = CCinES(CollisionOnReceiver{i});
	disp(['Collisoin on ',num2str(i)]);
	CollisionPair = CollisionOnReceiver{i};
	CollisionPair
	dGraph(CollisionPair(:,1),CollisionPair(:,2)) = dGraph(CollisionPair(:,1),CollisionPair(:,2)) +1;
    end
end
