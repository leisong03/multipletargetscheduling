close all;
regenPos = 0;
NumTargets = 4;
XYSize = [0 10 0 10];
ReceiverSize = [- 5 15 -5 15];
textOffset = [ 1 0];
MAX_TIME_MS = 100;

delta = 0.01;
d = 3;
R = 5;
ms = 10;
Omega = 1;
DeltaS =  1.3*sqrt(2)*R/ceil(sqrt(d+1));
[tx ty] = meshgrid([ReceiverSize(1) : DeltaS : ReceiverSize(2)], [ReceiverSize(3) : DeltaS : ReceiverSize(4)]);
XYReceivers = [tx(:) ty(:)];
figureHandle1 = figure();
figureHandle2 = figure();
if regenPos 
    trail = 0 
    while 1
	trail = trail + 1
	XYTargets =repmat([XYSize(2)-XYSize(1) XYSize(4)-XYSize(3)], NumTargets,1).*rand(NumTargets,2) +repmat([XYSize(1),XYSize(3)],NumTargets,1);
	DistMat = pdist2(XYTargets, XYReceivers);
	AudibleRX = DistMat <= R;
	if ~nnz(sum(AudibleRX,2)<=d+1)
	    break;
	end
    end
end
AudibleRX = DistMat <= R;
%[NumFigure, dGraph] = SaveRange(XYReceivers,XYTargets,R, Omega);
%T = [0,0,4*Omega];
%T = [0,4*Omega,0];
%T = [1.5*Omega,0,0];
%T = [0,0,0,0];
stageIdx = 0;
T = zeros(1,NumTargets);
while 1
    stageIdx = stageIdx  +  1
    [ conflictOnRx,conflictGraph] = CalConflict(T,XYReceivers,XYTargets,R,Omega);
    figure(figureHandle1);
    clf();
    hold on
    needLegend = 0;
    if needLegend 
	plot (inf,inf,'ko');
	plot (inf,inf,'kv');
	legend('Rx','Targets','location','bestoutside');
    end
    colorMap = hsv(size(XYReceivers,1));
    conflictOn = ~cellfun(@isempty,conflictOnRx);
    thisAudibleRx = AudibleRX;
    for i = find(conflictOn)
	conflictMat = conflictOnRx{i}; 
	for j = 1 : size(conflictMat,1)
	    thisAudibleRx(conflictMat(j,1),i) = 0;
	end
    end
    if ~nnz(sum(thisAudibleRx,2)<d+1)
	break;
    end

    for i = 1 : size(XYReceivers,1)
	%text(XYReceivers(i,1)+textOffset(1),XYReceivers(i,2)+textOffset(2),[num2str(i)],'color',colorMap(i,:));
	if conflictOn(i)
	    plot(XYReceivers(i,1),XYReceivers(i,2),'ko','markersize',ms,'MarkerFaceColor','k');
	    text(XYReceivers(i,1),XYReceivers(i,2),['R',num2str(i)],'color','k');
	else
	    plot(XYReceivers(i,1),XYReceivers(i,2),'ko','markersize',ms);
	end
    end
    circleAngle = linspace(0,2*pi,30)';
    circlePoint = R*[cos(circleAngle),sin(circleAngle)];
    colorMap = hsv(NumTargets);
    for i = 1 : NumTargets
	text(XYTargets(i,1)+textOffset(1),XYTargets(i,2)+textOffset(2),[num2str(i)],'color',colorMap(i,:));
	plot(XYTargets(i,1),XYTargets(i,2),'v','color',colorMap(i,:),'markersize',ms);
	plot(circlePoint(:,1)+XYTargets(i,1),circlePoint(:,2)+XYTargets(i,2),'--','color',colorMap(i,:));
    end
    [to from ] = find(conflictGraph);
    for i =  1 : length(from)
	arrow(XYTargets(from(i),:),XYTargets(to(i),:));
    end
    hold off;
    box off;
    axis off;
    axis equal;
    axis(ReceiverSize);
    sdf('pdf');
    SavePDFto(['figure/TargetAndRx_',num2str(stageIdx)],gcf);

    %SceneHandle = figure;
    %DrawScene(XYTargets,XYReceivers,R,dGraph,SceneHandle,Omega);
    figure(figureHandle2);
    clf();
    ConflictRX = ~cellfun(@isempty,conflictOnRx);
    cnt = 0; 
    for i = find(ConflictRX)
	cnt = cnt +1;
	handles = subplot(nnz(ConflictRX),1,cnt);
	ylabel(['Rx ',num2str(i)]);
	set(handles,'YTick',[]);
	hold on;
	thisRange = conflictOnRx{i};
	dashboard = zeros(1,NumTargets);
	lineBase = 0;
	for j = 1 : size(conflictOnRx{i},1)
	    from =  thisRange(j,2);
	    to =  thisRange(j,1); 
	    RangeFrom = DistMat(from,i);
	    RangeTo = DistMat(to,i);
	    if dashboard(from) == 0 
		lineBase = lineBase + 1;
		dashboard(from ) =  1;
		timeSeq = 0 : 0.01 : 50;
		waveSeq = zeros(size(timeSeq));
		onTime = (timeSeq>= RangeFrom/0.34+T(from))&(timeSeq<= RangeFrom/0.34+T(from)+Omega); 
		waveSeq(onTime) = 1;
		plot(timeSeq,waveSeq*.4+lineBase*.5,'color',colorMap(from,:));
	    end
	    if dashboard(to) == 0 
		lineBase = lineBase + 1;
		dashboard(to) =  1;
		timeSeq = 0 : 0.1 : 50;
		waveSeq = zeros(size(timeSeq));
		onTime = (timeSeq>= RangeTo/0.34+T(to))&(timeSeq<= (RangeTo/0.34+T(to)+Omega)); 
		waveSeq(onTime) = 1;
		plot(timeSeq,waveSeq*.4+lineBase*0.5,'color',colorMap(to,:));
	    end
	end
    end
    xlabel('time(ms)');
    sdf('pdf');
    SavePDFto(['figure/ConflictOnTime_',num2str(stageIdx)],gcf);

    needAdjust = (sum(thisAudibleRx,2)<d+1)'
    canBeConflict = zeros(1,NumTargets);
    minDelay = ones(1,NumTargets)*inf;
    for i = find(needAdjust)
	aviableTime = 0 : 0.01 : MAX_TIME_MS;
	isAvable = ones(size(aviableTime ));
	neighbourRx = AudibleRX(i,:)
	for j = find(neighbourRx)
	    possibleTag = AudibleRX(:,j)'
	    possibleTag(i) = 0; %excluse my self
	    %isAvable(aviableTime<DistMat(i,j)/.34+T(i)) = 0;
	    offset = DistMat(i,j)/.34+T(i);
	    for k = find(possibleTag)
		startTime = DistMat(k,j)/.34+T(k)-(~canBeConflict(k))*Omega;
		endtime = DistMat(k,j)/.34+T(k)+Omega+delta;
		isAvable(aviableTime>=(startTime-offset)& aviableTime<=(endtime-offset)) = 0;
	    end
	end
	minDelay(i) = aviableTime(find(isAvable,1,'first'));
    end
    minDelay(needAdjust)/Omega
    find(needAdjust)
    minIdx = find(minDelay == min(minDelay),1,'first');
    T(minIdx) = minDelay(minIdx)+T(minIdx);
    T
end
