function [CurLoc,Residual,Flags] = ChorusLocating(LastLoc,MotionRadius,DirtyDistMat,XYReceivers)
% ChorusLocating contains both SetDivision and locaitng in chorus
% @Parameters 
%	LastLoc is m x 2 matrix holding the last location 
%	MotionRadius is m x 1 matrix holding the motion radius of target
%	DirtyDistMat is m'x n matrix holding the dirty distanc measurement
%	XYReceiver is n x 2 matrix, holding the location of receivers
% @Return value
%	CurLoc is a mx2 location show currentlocation 
%	Residual is a mx1 location shows the locating error 
%FeasibleDistArray = cell(size(DirtyDistMat));
%m' x n matrix, holding the distance from targets to receivers
TagToRx = pdist2(LastLoc,XYReceivers); 
AssociationCube = zeros(size(DirtyDistMat,1),size(DirtyDistMat,2),size(LastLoc,1));
CurLoc = zeros(size(LastLoc));
Residual = ones(size(LastLoc,1),1).*inf;
Flags = ones(size(LastLoc,1),1).*inf;
GroupSize = 4;

for i = 1 : size(LastLoc,1)
    for j = 1 : size(DirtyDistMat,2)
	AssociationCube(:,j,i) = arrayfun(@(x)(x<TagToRx(i,j)+MotionRadius(i)&x>TagToRx(i,j)-MotionRadius(i)),DirtyDistMat(:,j)); 
    end
end
for i = 1: size(LastLoc,1)
    FeasibleRange = sum(AssociationCube(:,:,i));
    %upper bound of feasible location

    if nnz(FeasibleRange) < GroupSize
	%do nothing
    else 
	FeasibleLocUB = nchoosek(nnz(FeasibleRange),GroupSize).*(max(FeasibleRange).^GroupSize);
	FeasibleLocNum = 0;
	ThisLocV = ones(FeasibleLocUB,size(LastLoc,2)); % FeasibleRange x 2 
	ThisResV = ones(FeasibleLocUB,1);
	ThisFlagV = ones(FeasibleLocUB,1);

	RangableRx = find( FeasibleRange > 0);
	RangableRxComb = nchoosek(RangableRx,GroupSize);
	for j = 1 : size(RangableRxComb,1) % go through all possible range combination
	    RangeSet = cell(1,GroupSize);
	    for k = 1 : GroupSize;
		ThisColumn = DirtyDistMat(:,RangableRxComb(j,k));
		ThisColumnAssociation = AssociationCube(:,RangableRxComb(j,k),i);
		ThisColumn(ThisColumnAssociation ~= 0);
		%ThisColumn(find(ThisColumnAssociation));
		%RangeSet{k} = ThisColumn(find(ThisColumnAssociation));
		RangeSet{k} = ThisColumn(ThisColumnAssociation ~= 0);
	    end
	    RangeComb = allcomb(RangeSet{1 : GroupSize});
	    for ii = 1 : size(RangeComb,1)
		[ThisEst,Flag,Res] = ToALoc (XYReceivers(RangableRxComb(j,:),:),RangeComb(ii,:)');
		FeasibleLocNum = FeasibleLocNum + 1;
		ThisLocV(FeasibleLocNum,:) = ThisEst';
		ThisResV(FeasibleLocNum) = Res;
		ThisFlagV(FeasibleLocNum) = Flag;
	    end
	end
	MinIdx = find(ThisResV == min(ThisResV),1,'first');
	CurLoc(i,:) = ThisLocV(MinIdx,:); 
	Residual(i) = ThisResV(MinIdx);
	Flags(i) = ThisFlagV(MinIdx);
    end
end

end
