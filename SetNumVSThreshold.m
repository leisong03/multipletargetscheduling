function [ SetNumVec ] = SetNumVSThreshold( Threshold,NumTrace )
%NumTrace = 200;
StepNum = 100;
MuV = 2; % mean velocity
SigmaV = 0.1; %sigma of velocity 
SamplingRate = 10; %Sample rate sample/second
XBox = 10; % motion region 10 meters 
YBox = 10; % motion region 10 meters
TotalSec = 10; % simulation time
TurnSec = 3; % interval between random truning 
NumTargets = 7;
SamplingTime = 0 : 1/SamplingRate : TotalSec;
StepNum = length(SamplingTime);
TraceSet = zeros(StepNum,2,NumTrace);

for i = 1 : NumTrace
    %[x y] = GiveMeATrace(StepNum, 0.2);
    %[x y] = GiveMeATrace(StepNum, 0.2);
    [ x,y ] = GiveMeATrace(MuV,SigmaV, SamplingRate, XBox, YBox,TotalSec,TurnSec);
    TraceSet(:,:,i)  = [ x y]; 
end
SetNumVec = zeros(StepNum,1);
for t = 1 : StepNum
    CurrentLoc = zeros(NumTrace,2);
    for i = 1 : NumTrace
	CurrentLoc(i,:) = TraceSet(t,:,i);
    end
    [SetIdx SetNum] = SetDivision(CurrentLoc,Threshold);
    SetNumVec(t) = SetNum;
end
