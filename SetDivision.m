function [ IdxPoints, NumSet ] = SetDivision( XYPoints, Threshold )
%SETDIVISION Summary of this function goes here
%   Detailed explanation goes here
NumPoints = size ( XYPoints, 1);%10
IdxPoints = zeros ( NumPoints, 1);%zero(10,1) 
NumSet = 0; 
while nnz( IdxPoints == 0) >0 % untile all points are assigned to certain set
    while(1)
	UnassignedSet = find (IdxPoints == 0); %find out all unassigned points 10
	[FromIdx ToIdx] = meshgrid( UnassignedSet, UnassignedSet);%10X10
	FromIdx = triu(FromIdx,1);
	FromIdx = FromIdx(find(FromIdx >0));
	ToIdx = triu(ToIdx,1);
	ToIdx = ToIdx(find(ToIdx >0));
	DifferX = XYPoints(FromIdx,1) -XYPoints(ToIdx,1);
	DifferY = XYPoints(FromIdx,2) -XYPoints(ToIdx,2);
	Distance = sqrt(DifferX.^2 + DifferY.^2);
	if nnz( Distance < Threshold ) == 0 %No pair has distance smaller than threshold
	    break;
	else
	    MinDistanceIdx = find (Distance == min(Distance), 1,'first');
	    IdxPoints(FromIdx(MinDistanceIdx))  = -1;
	end
    end
    NewSetIdx = find ( IdxPoints == 0 );
    NumSet = NumSet + 1;
    IdxPoints( NewSetIdx ) = NumSet;
    NewSetIdx = find ( IdxPoints == -1 );
    IdxPoints( NewSetIdx ) = 0;
end
end

