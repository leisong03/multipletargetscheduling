function [ x,y ] = GiveMeATrace(MuV,SigmaV, SamplingRate, XBox, YBox,TotalSec,TurnSec)
%GIVEMEATRACE Summary of this function goes here
%   Detailed explanation goes here
SamplingTime = 0 : 1/SamplingRate : TotalSec;
XYNum = length(SamplingTime);
TurnEverySample = floor(TurnSec*SamplingRate);
xy = zeros(XYNum,2);
xy(1,:) = rand(1,2).*[XBox,YBox];
Dir = rand(1).*2*pi;
for i = 2 : XYNum
    if ~mod(i,TurnEverySample); % Turn direction every TurnNum points
	Dir  = wrapTo2Pi(Dir + rand(1).*pi);
    end
    StepLength =  normrnd(MuV,SigmaV)/SamplingRate;
    while StepLength < 0
	StepLength =  normrnd(MuV,SigmaV)/SamplingRate;
    end
    SmallDir = (rand()-0.5).*pi;
    [XStep,YStep] = pol2cart(Dir+SmallDir, StepLength);
    xy(i,:) = xy(i-1,:) + [XStep YStep];
    if xy(i,1) < 0  
	xy(i,1) = -1*xy(i,1);
	Dir = pi - Dir;
    end
    if xy(i,2) < 0 
	xy(i,2) = -1*xy(i,2);
	Dir = 2*pi - Dir;
    end

    if xy(i,1) > XBox 
	xy(i,1) = 2*XBox - xy(i,1);
	Dir = pi - Dir; 
    end
    if xy(i,2) > YBox
	xy(i,2) = 2*YBox - xy(i,2);
	Dir = 2*pi - Dir; 
    end
    Dir = wrapTo2Pi(Dir);
end
x = xy(:,1);
y = xy(:,2);
end
