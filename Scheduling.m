function [ conflictOn,conflictGraph ] = Scheduling(T,XYReceiver, XYTarget ,Radius, Omega)
    %SCHEDULING Summary of this function goes here
    %   Detailed explanation goes here
    %T = zeros( size(XYTarget,1),1);
    %FileName = '/Users/bean/Documents/MyPaper/[2015][W][Infocom]UltraChorusWithDelay/fig/ThisUDG'
    FileName = '.\figure\'
    %FileName = 'C:\Users\Lei\Documents\MyPapers\jasc2014-ultrachorus\fig\ThisUDG'
    DistMat = pdist2(XYTarget, XYReceiver);
    conflictGraph = zeros(size(XYTarget,1),size(XYTarget,1));
    A = sum(DistMat <= Radius)>1;
    conflictOn = zeros(size(A));
    for i = find(A)
	distToi = DistMat(:,i)+T'*340;
	distDiff = bsxfun(@minus,distToi,distToi');
	conflictGraph = conflictGraph + (distDiff>0 & distDiff < Omega);
	[pairA pairB] = find(distDiff>0 & distDiff < Omega);
	if nnz(distDiff>0 & distDiff < Omega)
	    conflictOn(i) = conflictOn(i) + 1;
	    conflictPair = [pairA pairB]
	    [distToi(pairA),distToi(pairB),distToi(pairA)-distToi(pairB)]
	end
    end
end
