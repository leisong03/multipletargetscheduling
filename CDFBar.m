function [ NumCurve ] = CDFBar( FileData,FileOutput)
%CDFBAR Summary of this function goes here
%   Detailed explanation goes here
    %OmegaV = [0 .1 .2 .5 1 1.5 2 5];
    %NoiseV = [0.0 0.01 0.05 0.1 0.2 0.5]; 

    OmegaV = [0,linspace(0.1,5,10.1)];
    NoiseV = linspace(0,0.5,10); 

    ACYMat = [];
    NumCurve = 0;
    fid = fopen(FileData,'r');
    h = figure();
    while ~feof(fid)
	line = fgets(fid);
	AA = sscanf(line,'%f ',2);
	Omega = AA(1);
	Noise = AA(2);
	line = fgets(fid);
	X = sscanf(line,'%f');
	line = fgets(fid);
	F = sscanf(line,'%f');
	idx = find(F>=0.9,1,'first');
	%ACY = interp1(X,F,0.9,'linear');
	ACY = X(idx); 
	ACYMat = [ACYMat; [Omega, Noise,ACY]];
	whos X;
	whos F;
    end
    ValidIdx = find(ACYMat(:,1) ~= -1.0 | ACYMat(:,2) ~= 0);
    ACYMat = ACYMat([1;ValidIdx],:);
    ACYMat(ACYMat(:,1) == -1, 1) = 0;
    ACYMat
    ZeroIdx = ACYMat(ACYMat(:,1)==0, :);
    NonZeroIdx = ACYMat(ACYMat(:,1)~=0, :);
    ACYMat =  [ZeroIdx(1:length(NoiseV),:);NonZeroIdx]
    ReshapeZ = reshape(ACYMat(:,3),length(NoiseV),length(OmegaV));
    whos ACYMat;
    surf(OmegaV,NoiseV,ReshapeZ);
    %plot3(ACYMat(:,1),ACYMat(:,2),ACYMat(:,3),);
    xlabel('\omega(m)');
    ylabel('noise(m)');
    sdf('pdf_20');
    SavePDFto(FileOutput,h);
    close(h);
end

