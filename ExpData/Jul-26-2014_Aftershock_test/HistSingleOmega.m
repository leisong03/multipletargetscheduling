close all;
load('distance_data');
X = [0:.1: 5] 
[N] = hist(distance/3,X);
bar(X,N./sum(N))
sigma = std(distance/3)/2;
mu = mean(distance/3);
hold on;
ezplot(@(omega)normpdf(omega,mu,sigma));
axis([-0.1 5 0 0.3]);
sdf('pdf_20');
xlabel('\omega(m)');
ylabel('Ratio');
SavePDFto('~/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/DistOmegaSingle',gcf);
