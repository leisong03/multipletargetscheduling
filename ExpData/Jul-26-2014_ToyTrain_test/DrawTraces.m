load('xyz.mat'); % load xyz to workspace
load('xyzreal.mat'); %load OutV to workspace
plot(xyz(:,1),xyz(:,2),'r-^',OutV(:,1),OutV(:,2),'g-');
axis equal;
xlabel('x(m)');
ylabel('x(m)');
sdf('pdf');
legend('Tracking result','Real trace','Location','Best');
SavePDFto('~/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/RealVSlocating',gcf);

DistanceV = V2VClose(xyz,OutV);
ecdf(DistanceV);
xlabel('Locating error(cm)');
ylabel('Ratio');
sdf('pdf');
SavePDFto('~/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/SingleTagCDF',gcf);
