close all;
clear
load('xyzreal');
xyz = dlmread('data1.txt');
plot(xyz(:,2),xyz(:,3),'r-^',OutV(:,1),OutV(:,2),'g-');
axis equal;
axis ([-250 200 -200 250]);
xlabel('x(cm)');
ylabel('x(cm)');
sdf('pdf_20');
legend('Tracking result','Real trace','Location','NorthWest');
SavePDFto('~/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/SingleTagTrace_1',gcf);

xyzOuter = dlmread('data1.txt');
xyz = dlmread('data2.txt');
xyz = xyz + repmat([0 5 0 0],size(xyz,1),1);
StaticA = [normrnd(25,0.1,length(xyzOuter),1),normrnd(50,0.1,length(xyzOuter),1)];
StaticB = [normrnd(30,0.1,length(xyzOuter),1),normrnd(110,0.1,length(xyzOuter),1)];

lengthxyzOuter = size(xyzOuter,1)
lengthxyz= size(xyz,1)
SetNumV = zeros(lengthxyz,1);
for i = 1 : length(SetNumV)
    FourSpots = [xyzOuter(i,2:3); xyz(i,2:3); StaticA(i,:);StaticB(i,:)];
    [idx,SetNumV(i)] = SetDivision(FourSpots,45)
end

m = 4;
plot(xyzOuter(1:m:size(xyzOuter,1),2),xyzOuter(1:m:size(xyzOuter,1),3),'k-v',xyz(1:m:size(xyz,1),2),xyz(1:m:size(xyz,1),3),'r-^',OutV(1:m:size(OutV,1),1),OutV(1:m:size(OutV,1),2),'g-',StaticA(1:m:size(StaticA,1),1),StaticA(1:m:size(StaticA,1),2),'y-<',StaticB(1:m:size(StaticB,1),1),StaticB(1:m:size(StaticB,1),2),'m->');
%plot(xyzOuter(:,2),xyzOuter(:,3),'k-v',xyz(:,2),xyz(:,3),'r-^',OutV(:,1),OutV(:,2),'g-',StaticA(:,1),StaticA(:,2),'y-<',StaticB(:,1),StaticB(:,2),'m->');
axis equal;
axis equal;
axis ([-250 200 -200 250]);
xlabel('x(cm)');
ylabel('x(cm)');
sdf('pdf_20');
legend('Tag on Pedestrain','Tag on Toy train','trace of toy train','Static Tag A','Static Tag B','Location','SouthWest');
SavePDFto('~/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/SingleTagTrace_2',gcf);

TReuse = 4./SetNumV;
TReuse(1:4) = 1;
plot([1:length(SetNumV)],TReuse,'r-',[1:length(SetNumV)],ones(length(SetNumV),1),'g-');
xlabel('Locating round');
ylabel('TimeSlot reuse');
legend('Exclusive Mode','Chorus Mode','Location','SouthEast');
sdf('pdf_20');
axis([0 200 0 5]);
SavePDFto('~/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/TimeSlotReuse',gcf);

DistanceV = V2VClose([xyz(:,2),xyz(:,3)],OutV);
[F2,X2] = ecdf(DistanceV);

xyz = dlmread('data3.txt');
xyz = xyz + repmat([0 5 0 0],size(xyz,1),1);
idx_1 = find(xyz(:,1) == 1);
idx_4 = find(xyz(:,1) == 4);
StaticA = [normrnd(25,0.1,length(idx_1),1),normrnd(50,0.1,length(idx_4),1)];
StaticB = [normrnd(30,0.1,length(idx_1),1),normrnd(110,0.1,length(idx_4),1)];
plot(xyz(idx_1,2),xyz(idx_1,3),'k-v',xyz(idx_4,2),xyz(idx_4,3),'r-^',OutV(:,1),OutV(:,2),'g-',StaticA(:,1),StaticA(:,2),'y-<',StaticB(:,1),StaticB(:,2),'m->');
%plot(xyz(idx_4,2),xyz(idx_4,3),'r-^',OutV(:,1),OutV(:,2),'g-');
axis equal;
axis ([-250 200 -200 250]);
xlabel('x(cm)');
ylabel('x(cm)');
sdf('pdf_20');
%legend('Tracking result','Real trace','Location','NorthWest');
legend('Tag on Pedestrain','Tag on Toy train','trace of toy train','Static Tag A','Static Tag B','Location','SouthWest');
SavePDFto('~/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/SingleTagTrace_3',gcf);

xyz = dlmread('data4.txt');
xyz = xyz + repmat([0 5 0 0],size(xyz,1),1);
idx_1 = find(xyz(:,1) == 1);
idx_4 = find(xyz(:,1) == 4);
plot(xyz(idx_1,2),xyz(idx_1,3),'r-^',xyz(idx_4,2),xyz(idx_4,3),'b-v',OutV(:,1),OutV(:,2),'g-');
%plot(xyz(idx_4,2),xyz(idx_4,3),'r-^',OutV(:,1),OutV(:,2),'g-');
axis equal;
axis ([-250 200 -200 250]);
xlabel('x(cm)');
ylabel('x(cm)');
sdf('pdf_20');
legend('Tracking result','Real trace','Location','NorthWest');
SavePDFto('~/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/SingleTagTrace_4',gcf);

DistanceV = V2VClose([xyz(idx_4,2),xyz(idx_4,3)],OutV);
[F1,X1] = ecdf(DistanceV);

%plot([X1;X2(end)],[F1;1],'r-',X2,F2,'g-');
%legend('Exclusive Mode','Chorus Mode','Location','SouthEast');
%xlabel('Locating error(cm)');
%ylabel('Ratio');
%axis([0 15 0 1.1]);
%sdf('pdf_20');
%SavePDFto('~/Documents/MyPaper/[2014][W][JSAC]UltraChorus/fig/MultipleTagCDF',gcf);


