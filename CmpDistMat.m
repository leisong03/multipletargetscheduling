DistMat
DirtyDistMat
ShortDistMat = ones(size(DistMat<R)).*inf;
ShortDistMat(DistMat<R) = DistMat(DistMat<R);

ValidColumIdx = find(sum(ShortDistMat<inf),5,'first');
BeforeMat = ShortDistMat(:,ValidColumIdx);
BeforeMat(BeforeMat == inf) = 0;

NumValidRow = max(sum(DirtyDistMat<inf));
AfterMat = DirtyDistMat(:,ValidColumIdx);

data=rand(3,4);
hold on;
BeforeName = cell(1,length(ValidColumIdx));
AfterName = cell(1,length(ValidColumIdx));
for i = 1 : length(ValidColumIdx)
    BeforeName{i} = ['R',num2str(i)];
    AfterName{i} = [' R',num2str(i)];
end

subplot(2,1,1)
alldata{1} = {1:2:2*length(ValidColumIdx), BeforeMat,'BASE','XLABEL',BeforeName };
bar2(alldata{1});
title('True value');
ylabel('Range(m)');

subplot(2,1,2)
alldata{2} = {2:2:2*length(ValidColumIdx), AfterMat,'BASE','XLABEL',AfterName };
bar2bw(alldata{2});
title('After anonymous and collison process');
ylabel('Range(m)');

sdf('font_20');
