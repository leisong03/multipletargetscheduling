function [ DirtyDistMat,RankMat ] = AnonyCollison( CleanDistMat, omega, Radius )
%ANONYCOLLISON Change the distance matrix to anonymous and collision form
%   CleanDistMat: is the normal distance matrix, distance to receiver i is in ith column.
%   Distance from the target j is in jth row 
%   DirtyDistMat: is the anonymous distance matrix and processed by collision noise
[SortedMat,FirstRankMat] = sort( CleanDistMat,1,'ascend' );
SortedMat(SortedMat > Radius) = inf;
%add collsion 
CountV = zeros(size(CleanDistMat,2),1);
for i = 1 : size(SortedMat,2) %Column by column
    j = 1;
    while (  j <= size(SortedMat,1) && SortedMat(j,i) < inf)
	if (j > 1 && SortedMat(j,i) - SortedMat(j-1,i) < omega)
	    SortedMat(j,i) = inf;
	    CountV(i) = CountV(i) + 1;
	end
	j = j + 1;
    end
end
[SortedMat,SecRankMat] = sort(SortedMat,1,'ascend');
DirtyDistMat = SortedMat;
RankMat = zeros(size(SecRankMat));
for i = 1 : size(RankMat,2)
    for j = 1 : size(RankMat,1)
	RankMat(j,i) = FirstRankMat(SecRankMat(j,i) ,i) ;
    end
end
end

