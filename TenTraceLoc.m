%Threshold = [1 2 5 8 10 20 ]*0.33;
Threshold = 0.25;
Num = 1:5:50;
ColorString =hsv(length(Num)); 
figdir = 'figure/';

close all 
DistFig = figure();
hold on;
cidx = 1;
%LegendVec = cell(length(Threshold),1);
LegendVec = cell(length(Num),1);
NumSetVsNum = zeros(size(Num));
for i = Num
    i
    LegendVec{cidx} = ['n =',num2str(i)]; 
    y = SetNumVSThreshold (Threshold,i);
    %y = 50./y;
    %plot(y,'color',ColorString(cidx,:));
    NumSetVsNum(cidx) =  i/mean(y);
    cidx = cidx+1;
end
%TOACDelay = NumSetVsNum;
ChorusDelay = NumSetVsNum;
plot(Num,[NumSetVsNum;Num]);
%plot(Num,Num);
%legend();
sdf('pdf');
xlabel('Num of targets');
ylabel('refreshing rate');
SavePDFto('ReuseVSOmega',gcf);
%saveas(gcf,ReuseVSOmega'],'pdf')
%command = ['/bin/bash', ' --login',' ./figure/a.sh',' ./ReuseVSOmega.pdf']; 
%system(command);
