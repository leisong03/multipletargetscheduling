function [ epsilon ] = MaxDistInInter( Bcenter, Radius )
%MAXDISTININTER Summary of this function goes here
%   Detailed explanation goes here
epsilon = 0;
theta = linspace(0,2*pi,1000)';
%CircleA = Radius.*[cos(theta),sin(theta)];
CircleB = Radius.*[cos(theta),sin(theta)] + repmat(Bcenter,length(theta),1);
DistBToA = pdist2([0,0],CircleB);
InIdx = find(DistBToA < Radius);
if isempty(InIdx)
    epsilon = 0
    return;
end
DtoA = pdist2([0,0],CircleB(InIdx,:));
DtoB = pdist2(Bcenter,CircleB(InIdx,:));
difference = abs(DtoA - DtoB); 
epsilon = max(difference);
end

