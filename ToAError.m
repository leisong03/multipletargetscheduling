close all;
ColorStr = 'bgrcmyk';
FileName = '/Users/bean/Documents/MyPaper/[2015][W][Infocom]UltraChorusWithDelay/fig/ToAerror'
omega = 0.5
XOuter = 10;
YOuter = 10;
[tx ty] = meshgrid([0:3:XOuter],[0:3:YOuter]);
Radius = 3.2;
XYRx = [tx(:),ty(:)] + rand(size([tx(:),ty(:)]));
NumTag = 20;
XIn = 6;
YIn = 6;
XYTag = rand(NumTag,2).*repmat([XIn,YIn],NumTag,1)+repmat([XOuter-XIn YOuter-YIn]./2,NumTag,1);
DistMat = pdist2(XYTag,XYRx);
EstXYTag = zeros(size(XYTag));
noise = [0,0.01,0.02,0.05,0.1,0.2,0.5];
hold on;
for j = 1 : length(noise);
    EstError = [];
    for i = 1 : size(XYTag,1)
	IdxReachRx = find (DistMat(i,:) < Radius);
	thisnoise = rand(1,length(IdxReachRx)).*noise(j);
	[ThisEst,Flag,Res] = ToALoc(XYRx(IdxReachRx,:),DistMat(i,IdxReachRx)'+thisnoise'); 
	EstXYTag(i,:) = ThisEst';
	EstError = [EstError; norm(EstXYTag(i,:)-XYTag(i,:))];
    end
    [f,x] = ecdf(EstError);
    plot([x;1.5],[f;1],['-',ColorStr(j)]);
end
legend('0','0.01','0.02','0.05','0.1','0.2','0.5');
xlabel('error(m)');
ylabel('Ratio');
sdf('pdf_20');
SaveSuccess = SavePDFto(FileName, gcf);
close all;
return

theta = linspace(0,2*pi,50)';
hold on;
[DirtyMat,RankMat] = AnonyCollison(DistMat,omega,Radius)
for i = 1 : size(DistMat,2)
    for j = 1 : nnz(DirtyMat(:,i) < inf)
	XYCircle = DirtyMat(j,i).*[cos(theta),sin(theta)]+repmat(XYRx(i,:),length(theta),1);
	plot(XYCircle(:,1),XYCircle(:,2),[ColorStr(RankMat(j,i)),'-']);
    end
end
plot(XYTag(:,1),XYTag(:,2),'k^')
box on;
axis equal;
%axis([0 XOuter 0 YOuter]);
xlabel('X(m)');
ylabel('Y(m)');
FileName = '/Users/bean/Documents/MyPaper/[2015][W][Infocom]UltraChorusWithDelay/fig/ToAerror2'

axis off;
sdf('pdf_20');
SaveSuccess = SavePDFto(FileName, gcf);
%close all;
