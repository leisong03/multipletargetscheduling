function [ NumCC, CCidx ] = CCinES( EdgeSet )
%   CCinES: finding connected component in GraphPresented as edges set
NumCC = 0;
CCidx = zeros(size(EdgeSet,1),1);
while nnz(CCidx == 0) 
    ThisElement = find( CCidx == 0 , 1);
    for j = 1 : NumCC
	ThisCC = find( CCidx == j);
	SetA = EdgeSet(ThisCC,:);
	SetA = SetA(:);
	LIA = ismember(SetA, EdgeSet(ThisElement,:));
	if nnz(LIA)
	    CCidx(ThisElement) = j;
	    break;
	end
    end
    if CCidx(ThisElement) == 0
	NumCC = NumCC + 1;
	CCidx(ThisElement) = NumCC;
    end
end
end

