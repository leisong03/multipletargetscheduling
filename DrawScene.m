function DrawScence(XYTargets, XYReceivers,R,dGraph,handle,ShowReceiver);
clf(handle);
XYoffset = [2,2];
Xmin = min([XYTargets(:,1);XYReceivers(:,1)]);
Ymin = min([XYTargets(:,2);XYReceivers(:,2)]);
Xmax = max([XYTargets(:,1);XYReceivers(:,1)]);
Ymax = max([XYTargets(:,2);XYReceivers(:,2)]);
theta = linspace(0,2*pi,100)
circle = R.*[cos(theta'),sin(theta')];
hold on;
RxName = cell(size(XYReceivers,1),1);
for i = 1 : size(XYReceivers,1)
    RxName{i} = ['R',num2str(i)]; 
end

TagName = cell(size(XYTargets,1),1);
for i = 1 : size(XYTargets,1)
    TagName{i} = ['T',num2str(i)]; 
end

plot(XYTargets(:,1),XYTargets(:,2),'^r','MarkerSize',15);
text(XYTargets(:,1),XYTargets(:,2),TagName);
if ShowReceiver
    plot(XYReceivers(:,1),XYReceivers(:,2),'ok','MarkerSize',15);
    text(XYReceivers(:,1),XYReceivers(:,2),RxName);
end
if isempty(dGraph)
    for i = 1 : size(XYTargets,1)
	thiscircle = circle + repmat(XYTargets(i,:),size(circle,1),1);
	plot(thiscircle(:,1),thiscircle(:,2),'--');
    end
end
legend('Targets','Receivers');
XYTargets
dGraph
if ~isempty(dGraph)
    gplotdc(dGraph,XYTargets);
end
hold off;
%plot(XYTargets(:,1),XYTargets(:,2),'*r',XYReceivers(:,1),XYReceivers(:,2),'.b');
xlabel ('x(m)');
ylabel ('y(m)');
sdf('pdf');
box on;
axis equal;
axis([Xmin Xmax Ymin Ymax]+[-1*XYoffset(1) XYoffset(2) -XYoffset(2) XYoffset(2)]);
