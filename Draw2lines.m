function [ NumCurve ] = Draw2lines( FileData,FileOutput)
%DRAW2LINES Summary of this function goes here
%   Detailed explanation goes here
NumCurve = 0;
ColorArray = 'bgrcmyk';
fid = fopen(FileData,'r');
h = figure();
hold on;
LegendArray = {};
clist=hsv(15);
First = 1;
maxx = 0;
Count = 0;
while ~feof(fid)
    if NumCurve > 6
	break;
    end
    line = fgets(fid);
    AA = sscanf(line,'%f',2);
    Omega = AA(1);
    Noise = AA(2);
    line = fgets(fid);
    X = sscanf(line,'%f');
    line = fgets(fid);
    F = sscanf(line,'%f');
    whos X;
    whos F;
    if Omega == -1 && First == 1;
	First =0;
	NumCurve = NumCurve + 1;
	LegendArray{NumCurve} = 'Exclusive, N = 0 ';
	plot(X,F,'-.','col',clist(NumCurve,:));
    end
    if Omega ~= -1
	NumCurve = NumCurve + 1;
	LegendArray{NumCurve} = ['\omega = ',num2str(Omega),' N = ',num2str(Noise)];
	plot(X,F,'col',clist(NumCurve,:));
	if maxx < max(X)
	    maxx = max(X);
	end
    end
    Count = Count + 1
end
axis([0 maxx 0 1.05]);
legend(LegendArray,'Location','Best');
xlabel('locating error(m)');
ylabel('Distribution');
sdf('pdf_20');
SavePDFto(FileOutput,h);
close(h);
